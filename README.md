## Jira Issue Generator

### Requirements

- `node`

### Getting Started

- Run `yarn install`
- Create a `.env` based of the `.env-example` and populate the keys with their respective values

### To Generate Issues

- Run `yarn start`

### How It Works

Based on the defined `ISSUE_COUNT` environment variable, the generator will randomly create Epics, Tasks, and Subtasks, along with randomized associations to the various parent issues according to the Jira issue hierarchy - `Epic` > `Task` > `Subtasks`.

`Epic` names in Jira Next Gen projects are variable, you need to make sure you input the corresponding `Epic` name from the Jira Next Gen project in your `.env`. 

### To Do

- [X] Get basic API interactions working
- [X] Get the issue creation working in a minimal fashion
- [ ] Create templates for issue generation
- [ ] Add more complex data to issue templates such as custom fields, etc.
- [ ] Switch to bulk issue creation endpoint
- [ ] Make it completely agnostic to any project such that it builds issues from dynamic project configuarions.