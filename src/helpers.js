export function pickRandom(arr) {
  return arr[Math.floor(Math.random() * arr.length)]
}

export function handleError(err) {
  if (err.response) {
    console.log(err.response.data)
    console.log(err.response.status)
    console.log(err.response.headers)
  }

  if (err.message) {
    console.log('Error: ', err.message)
    return
  }

  console.log('Error Config: ', err.config)
  return
}