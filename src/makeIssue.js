import faker from 'faker'

import { pickRandom } from './helpers'

export function makeIssue({ issueType, parent, project, users }) {

  let fields = {
    summary: faker.lorem.sentence(),
    issuetype: {
      id: issueType.id
    },
    project: {
      id: project.id
    },
    reporter: {
      id: pickRandom(users)
    }
  }

  if (parent) {
    fields = {
      parent: {
        key: parent
      },
      ...fields
    }
  }

  return {
    fields
  }
}