import dotenv from 'dotenv'
import axios from 'axios'

import { handleError, pickRandom } from './helpers'
import { makeIssue } from './makeIssue'

dotenv.config()

const TEST_USERS = ['gweaver', 'kokeefe', 'dcook', 'jhope']
const JIRA_API = `${process.env.JIRA_URL}/rest/api/3`
const JIRA_CREDS = `${process.env.JIRA_EMAIL}:${process.env.JIRA_TOKEN}`
const JIRA_AUTH = Buffer.from(JIRA_CREDS).toString('base64')
const EPIC_NAME = process.env.EPIC_NAME

// A basic req function to make http requests to the Jira API
async function req({ url, method, params, data }) {
    const config = {
        method,
        baseURL: JIRA_API,
        url,
        data,
        params,
        headers: {
          Authorization: `Basic ${JIRA_AUTH}`,
          'Content-Type': 'application/json'
        }
    }

    let res 

    try {
      res = await axios(config)
    } catch (e) {
      handleError(e)
    }

    return res
}

// Collect accountIds that match our TEST_USERS we will create issues with
async function getUsers() {
  const res = await req({
    url: '/users/search',
    method: 'GET',
  })

  return res.data
    .filter(u => TEST_USERS.includes(u.displayName))
    .reduce((acc, curr) => [curr.accountId, ...acc], [])
}

// Get project info based on supplied project key
async function getProject() {
  const res = await req({
    url: `/project/${process.env.JIRA_PROJECT_KEY}`,
    method: 'GET',
  })

  return res.data
}

export async function generateIssues() {
  const users = await getUsers()
  const project = await getProject()

  let remainingIssues = process.env.ISSUE_COUNT
  
  // Array of IDs of created issues
  let epics = []
  let tasks = []
  let subtasks = []

  let parent = undefined

  // Loop creating issues until we've made the count defined in the env variable
  while (remainingIssues > 0) {
    // Pick a random issue type
    let issueType = pickRandom(project.issueTypes)

    // Ensure we don't create a subtask as the first type of issue
    if (tasks.length === 0 && issueType.subtask) {
      issueType = pickRandom(project.issueTypes.filter(t => !t.subtask))
      console.log('IN HERE')
    }

    // Ensure we create an epic first so future issues can have parents
    if (project.style !== 'classic' && epics.length === 0) {
      issueType = project.issueTypes.find(obj => obj.name === EPIC_NAME) || new Error('Incorrect Epic name mapping')
    }

    if (issueType.subtask) {
      parent = pickRandom(tasks)
    }

    if (issueType.name !== EPIC_NAME && !issueType.subtask) {
      parent = pickRandom(epics)
    }

    const data = makeIssue({ issueType, parent, project, users })

    console.log('ISSUE DATA:', data)
    console.log('Issue Type:', issueType)
    console.log('Parent: ', parent)
    console.log('Epics: ', epics)
    console.log('Tasks: ', tasks)
    console.log('Subtasks: ', subtasks)

    let res
    // Create the issue in Jira
    res = await req({
      url: '/issue',
      method: 'POST',
      data
    })
    
    // Add the id to the appropriate array
    switch (true) {
      case issueType.name === EPIC_NAME:
        epics.push(res.data.key)
        break
      case issueType.subtask:
        subtasks.push(res.data.key)
        break 
      default:
        tasks.push(res.data.key)
    }

    // Decrement the remaining issue count
    remainingIssues--
    parent = undefined
    
    // Log issue creation meta-data
    console.log(`Successfully Created ${issueType.name === EPIC_NAME && 'Epic' || issueType.name}: `, `${res.data.id}: ${res.data.key}.   ${remainingIssues} to go...`)
  }
}
